<?php /** @noinspection ALL */

// Prevents the browser from parsing this as HTML.
header('Content-Type: text/plain');


// There is no reason to stop the app execution if the log operation fails
try {
    $this->logOperation($operation);
} catch (\Exception $e) {
    Log::error('Cannot log operation:' . $e->getMessage());
}


/**
 * Generates an IN clause based on the number of params.
 * Function will return `?,?,?,?` for $numberOfParams=4
 */
function prepareInParams(int $numberOfParams)
{
    return \implode(',', \array_fill(0, $numberOfParams, '?'));
}

// This operation could take more that one hour.
function parsePostsXML(){}