<?php
public
function /*array*/ GetMultiSearch(
    /*string*/ $start_date,
    /*string*/ $end_date,
    /*string*/ $only_reinstall,

    /*array*/ $params,
    /*array*/ $values,

    /*string*/ $op,

    /*int*/ $start = false,
    /*int*/ $stop = false,
    /*string*/ $product_type = null,
    /*boolean*/ $export = null)
{

    //** Init Stuff
    $ret = array();
    $cond_sql = "";
    $cond_sql_second = "";
    $termA = "";
    $termB = "";
    /**
     * add support for old customers that are not migrated
     *
     * we have to look in sccustomers.customers table.
     * each customer record have a coresponding key in users_key tables.
     * we need to search in each of that tables, get the key_id and then the user email frm users table
     * the user email is passed to the main sql.
     */
    $oldCustomersSQl = "select cust.id as id from sccustomers.customers cust 
            join screport.users us on cust.container_id=us.id
            where us.path like '%/" . $_SESSION['user_id'] . "/%'";


    //$sqlOrder = " group by rt.email ";
    $sqlOrder1 = " group by registration_type_id order by cust.firstname,cust.lastname,rt.email ASC ";
    $sqlOrder2 = " order by cust.id DESC, cust.firstname,cust.lastname,cust.email ASC ";
    $sqlOrder3 = " order by s.id DESC, s.firstname,s.lastname,s.email ASC ";
    $SelectMainFields = "Select cust.container_id,cust.customer_id, cust.country, cust.zip, cust.state, 
                    cust.state, cust.city, cust.address2, cust.address,
                    cust.phone,cust.lastname,cust.firstname,aatu.activation_code_id,ac.code,
                    rt.email as reg_email,us.path, rt.id as registration_type_id, '' as email, us.group_name";

    $SelectSecondFields = "Select ac.id,cust.container_id,cust.customer_id, cust.country, cust.zip, 
                    cust.state, cust.city, cust.address2, cust.address,
                    cust.phone,cust.lastname,cust.firstname,ac.id as activation_code_id, ac.code as code,
                    rt.email as reg_email,us.path, rt.id as registration_type_id, cust.email, us.group_name";

    $selectFromSqlMain = "from sccustomers.registration_type rt
                    left join suid.activation_codes_to_users aatu on rt.id=aatu.registration_type_id
                    left join suid.activation_codes ac on aatu.activation_code_id=ac.id
                    left join suid.customers cust on ac.customer_id=cust.id 
                    left join screport.users us on cust.container_id=us.id";

    $selectFromSqlSecond = "from suid.customers cust
                    left join screport.users us on cust.container_id=us.id 
                    left join suid.activation_codes ac on cust.id=ac.customer_id
                    left join suid.activation_codes_to_users aatu on ac.id=aatu.activation_code_id
                    left join sccustomers.registration_type rt on aatu.registration_type_id=rt.id";

    $mainInitWhere = " where us.path like '%/" . $_SESSION['user_id'] . "/%' ";

    $mainSql = "$SelectMainFields
                    $selectFromSqlMain
                    $mainInitWhere";

    $countSql = "Select count(*) as nr
                    $selectFromSqlMain
                    $mainInitWhere";

    //*** Set the Limit -> remove ther limits for the full list (export)
    if ($stop) $limit_sql = "LIMIT  $start,$stop";
    else
        $limit_sql = "";


    $useEmailUnion = false;
    ////////////////////////////////

    //** List of allowed keywords
    $first_field = true;
    $keywords = array('partner', 'container_id', 'firstname', 'lastname', 'street_address', 'city', 'state', 'zip', 'phone', 'email', 'account_id', 'registration_date', 'account_no', 'skey', 'activation_code');
    //if(array_key_exists('container_id',$values)) {
    //	unset($values['container_id']);
    //}

    foreach ($values as $k => $val) {
        $max = count($values) - 1;
        $key = $params[$k];

        //if(strcmp($k,'container_id') == 0 ) {
        //	$key = $k;
        //}

        $val = trim($val);
        if (in_array($key, $keywords) && !empty($val)) {

            switch ($op[$k]) {
                case    'AND'    :
                    $sql_op = 'AND';
                    break;
                case    'OR'    :
                    $sql_op = 'OR';
                    $ph = '(';
                    break;
                default            :
                    $sql_op = '???';
                    break;
            }
            //** group keywords linked by OR useing "()" to overide MySql operators Precedence
            //--- init  (restet)
            $ph1 = "";
            $ph2 = "";

            //--- group start (
            if ($op[$k + 1] == 'OR' && !$group_start) {
                $group_start = true;
                $ph1 = "\n(";
                $ph2 = "";
            }

            //--- group end )
            if ($op[$k] == 'OR' && $op[$k + 1] != 'OR' && $group_start) {
                $group_start = false;
                $ph1 = "";
                $ph2 = ")";

            }

            if ($op[$k + 1] == 'OR' && $values[$k + 1] == '' && !$group_start) {
                $group_start = false;
                $ph1 = "";
                $ph2 = ")";

            }

            //---------------------------------------------------------------------------------

            if ($first_field) {
                $l_operator = "AND";
                $first_field = false;
            } else {
                $l_operator = $sql_op;
            }

            // This will add the slashes and replace the '*' with '%' to prepare the value for a <like> condition
            $val = mysql_escape_string($this->CreateWord($val));
//                    $val = str_replace('_', '\_', $val);
            $termVal = $this->likeOrEqualOnTerm($val);
            switch ($key) {
                case 'skey':
                    // search for key details and construct the query with customer information in
                    $keyPresent = $this->getKeyTypeIfExists($val);
                    $fieldValue = $val;
                    if (sizeof($keyPresent) > 0) {
                        $useRestUnion = true;
                        if (!empty($keyPresent['new_customer_id'])) {
                            $fieldValue = $keyPresent['new_customer_id'];
                        } else if (!empty($keyPresent['customer_id'])) {
                            $fieldValue = $keyPresent['customer_id'];
                        }
                    }

                    $cond_sql .= " $l_operator $ph1  cust.customer_id='" . $fieldValue . "' $ph2";
                    $cond_sql_second .= " $l_operator $ph1  cust.customer_id='" . $fieldValue . "' $ph2";
                    $cond_sql_second_old .= " $l_operator $ph1  cust.customer_id='" . $fieldValue . "' $ph2";
                    $termA .= " $l_operator $ph1  cust.customer_id='" . $fieldValue . "'  $ph2 ";
                    $termB .= " $l_operator $ph1  cust.customer_id='" . $fieldValue . "'  $ph2 ";

                    break;

                //** This one has to be cheched against 2 fields
                case 'street_address':
                    $cond_sql .= " $l_operator $ph1 (cust.address $termVal OR cust.address2 $termVal) $ph2";
                    $cond_sql_second .= " $l_operator $ph1 (cust.address $termVal OR cust.address2 $termVal) $ph2";
                    $cond_sql_second_old .= " $l_operator $ph1 (cust.address $termVal OR cust.address2 $termVal) $ph2";
                    $useRestUnion = true;
                    $termA .= " $l_operator $ph1 (cust.address $termVal OR cust.address2 $termVal) $ph2 ";
                    $termB .= " $l_operator $ph1 (cust.address $termVal OR cust.address2 $termVal) $ph2 ";
                    break;
                case 'email':
                    $cond_sql .= " $l_operator $ph1 (rt.email $termVal) $ph2";
                    $cond_sql_second .= " $l_operator $ph1 (cust.email  $termVal OR rt.email $termVal) $ph2";
                    $cond_sql_second_old .= " $l_operator $ph1 (cust.email  $termVal) $ph2";
                    $useEmailUnion = true;
                    $termA .= " $l_operator $ph1  cust.email $termVal $ph2 ";
                    $termB .= " $l_operator $ph1  rt.email $termVal $ph2 ";

                    break;
                case 'firstname':
                    $cond_sql .= " $l_operator $ph1  trim(cust.firstname) $termVal $ph2";
                    $cond_sql_second .= " $l_operator $ph1  trim(cust.firstname) $termVal $ph2";
                    $cond_sql_second_old .= " $l_operator $ph1  trim(cust.firstname) $termVal $ph2";
                    $useRestUnion = true;
                    $termA .= " $l_operator $ph1 trim(cust.firstname) $termVal  $ph2";
                    $termB .= " $l_operator $ph1 trim(cust.firstname) $termVal  $ph2";
                    break;
                case 'lastname':
                    $cond_sql .= " $l_operator $ph1  trim(cust.lastname) $termVal $ph2";
                    $cond_sql_second .= " $l_operator $ph1  trim(cust.lastname) $termVal $ph2";
                    $cond_sql_second_old .= " $l_operator $ph1  trim(cust.lastname) $termVal $ph2";
                    $useRestUnion = true;
                    $termA .= " $l_operator $ph1  trim(cust.lastname) $termVal  $ph2 ";
                    $termB .= " $l_operator $ph1  trim(cust.lastname) $termVal  $ph2 ";
                    break;
                case 'city':
                    $cond_sql .= " $l_operator $ph1  trim(cust.city) $termVal $ph2";
                    $cond_sql_second .= " $l_operator $ph1  trim(cust.city) $termVal $ph2";
                    $cond_sql_second_old .= " $l_operator $ph1  trim(cust.city) $termVal $ph2";
                    $useRestUnion = true;
                    $termA .= " $l_operator $ph1  trim(cust.city) $termVal $ph2 ";
                    $termB .= " $l_operator $ph1  trim(cust.city) $termVal $ph2 ";
                    break;
                case 'zip':
                    $cond_sql .= " $l_operator $ph1 cust.zip $termVal $ph2";
                    $cond_sql_second .= " $l_operator $ph1 cust.zip $termVal $ph2";
                    $cond_sql_second_old .= " $l_operator $ph1 cust.zip $termVal $ph2";
                    $useRestUnion = true;
                    $termA .= " $l_operator $ph1 cust.zip $termVal $ph2 ";
                    $termB .= " $l_operator $ph1 cust.zip $termVal $ph2 ";
                    break;
                case 'state':
                    $cond_sql .= " $l_operator $ph1 cust.state $termVal $ph2";
                    $cond_sql_second .= " $l_operator $ph1 cust.state $termVal $ph2";
                    $cond_sql_second_old .= " $l_operator $ph1 cust.state $termVal $ph2";
                    $useRestUnion = true;
                    $termA .= " $l_operator $ph1 cust.state $termVal $ph2 ";
                    $termB .= " $l_operator $ph1 cust.state $termVal $ph2 ";
                    break;
                case 'phone':
                    $cond_sql .= " $l_operator $ph1  trim(cust.phone) $termVal $ph2";
                    $cond_sql_second .= " $l_operator $ph1  trim(cust.phone) $termVal $ph2";
                    $cond_sql_second_old .= " $l_operator $ph1  trim(cust.phone) $termVal $ph2";
                    $useRestUnion = true;
                    $termA .= " $l_operator $ph1  trim(cust.phone) $termVal $ph2";
                    $termB .= "  $l_operator $ph1  trim(cust.phone) $termVal $ph2";
                    break;
                case 'registration_date':
                    $cond_sql .= " $l_operator $ph1 ac.registration_date LIKE '$val%' $ph2";
                    $cond_sql_second .= " $l_operator $ph1 ac.registration_date BETWEEN '" . $val . " 00:00:00' AND '" . $val . "  23:59:59' $ph2";
                    //$cond_sql_second	.= " $l_operator $ph1 ac.registration_date LIKE '$val%' $ph2";
                    $cond_sql_second_old .= "  $l_operator $ph1 ac.registration_date BETWEEN '" . $val . " 00:00:00' AND '" . $val . "  23:59:59' $ph2";
                    $useRestUnion = true;
                    $termA .= " $l_operator $ph1 ac.registration_date BETWEEN '" . $val . " 00:00:00' AND '" . $val . "  23:59:59' $ph2";
                    $termB .= " $l_operator $ph1 ac.registration_date BETWEEN '" . $val . " 00:00:00' AND '" . $val . "  23:59:59' $ph2";
                    break;
                case 'account_id':
                    $cond_sql .= " $l_operator $ph1 cust.account_id $termVal $ph2";
                    $cond_sql_second .= " $l_operator $ph1 cust.account_id $termVal $ph2";
                    $cond_sql_second_old .= " $l_operator $ph1 cust.account_id $termVal $ph2";
                    $useRestUnion = true;
                    $termA .= " $l_operator $ph1  cust.account_id $termVal  $ph2 ";
                    $termB .= " $l_operator $ph1  cust.account_id $termVal  $ph2 ";
                    break;

                case 'activation_code':
                    $joinCondition = true;
                    $joinTerm = "INNER JOIN activation_codes ac on cust.id = ac.customer_id ";
                    $termA .= " $l_operator $ph1  ac.code $termVal  $ph2 ";

                    break;
                case 'partner':

                    $joinCondition = true;

                    $termA .= " $l_operator $ph1  us.group_name = '$val'  $ph2 ";
                    $termB .= " $l_operator $ph1  us.group_name = '$val'  $ph2 ";

                    break;
            }
        }
    }
    //$keywords = array( 'account_id',	'account_no');
    if ($group_start) {
        $group_start = false;
        $cond_sql .= ")";
        $cond_sql_second .= ")";
    }


    //*** Encapsulate in () if the query is not empty
    if (!empty($cond_sql)) $cond_sql = " $cond_sql   ";
    if (!empty($cond_sql_second)) $cond_sql_second = " $cond_sql_second   ";
    $ret['found'] = 0;
    //$ret['rows_returned'] = 0;

    ////////////////////////////////

//        $oldQuery = $oldCustomersSQl.' AND '.$cond_sql_second_old;


    $cIds = array();
    $idList = '';
    // run old customers sql
//        $resultOld = mysql_query($oldQuery);
//        if($resultOld && mysql_num_rows($resultOld)>0) {
//            while($rowOld = mysql_fetch_assoc($resultOld)){
//                $cIds[] = $rowOld['id'];
//            }
//        }
//
//        mysql_free_result($resultOld);

    if (sizeof($cIds) > 0) {
        $idList = implode(',', $cIds);
    }
    //echo $idList.'<br />';
    //
    // try to get the user from one user table
    //
    $endAll = "";
    $oldSql = '';
    $startWhere = "";
    $endWhere = "";
    $customerSql = "";
    $emails = $this->getEmailFromUsersTable($idList);
    $totalEmails = sizeof($emails['emails']);
    $totalCustomer = sizeof($emails['customer']);
    $i = 1;
    $j = 1;

    if ($totalEmails > 0) {
        //
        // we have list of emails that match the old customers
        // add all to the sql where
        //
        $oldSql = " OR (rt.email IN(";
        $oldEmail = " rt.email in(";
        foreach ($emails['emails'] as $customer_id => $email) {
            $oldSql .= "'" . $email . "'";
            $oldEmail .= "'" . $email . "'";
            if ($i < $totalEmails) {
                $oldSql .= ",";
                $oldEmail .= ",";
            }
            $i++;
        }
        $oldEmail .= ")";
        $oldSql .= "))";
        $startWhere = "((";
        $endWhere = ")";
        $ret['old_customers'] = $emails;
    }

    if ($totalCustomer > 0) {
        $startWhere = "((";
        $customerSql = " OR (ac.id in(";
        foreach ($emails['customer'] as $customer_id => $datas) {
            $customerSql .= "'" . $datas['code'] . "'";
            if ($j < $totalCustomer) {
                $customerSql .= ",";
            }
            $j++;

            $ret['old_customers']['customer'][$customer_id] = $datas['new_customer'];
        }
        $customerSql .= "";

        if ($oldSql != '') {
            $customerSql .= ")) ";
        } else {
            $customerSql .= "))) ";
        }
    }


    if ($startWhere == "((") {
        $endAll = ")";
    }
///////////////////////
    /*
            $query = $mainSql . $cond_sql . $sqlOrder1. $limit_sql;

            $query2 = $countSql . $cond_sql.$sqlOrder1;
            $result = mysql_query($query);

            if($result && mysql_num_rows($result)>0) {
                while($row = mysql_fetch_assoc($result)){
                    $ret['results'][$row['registration_type_id']]['registration_type_id']		= $row['registration_type_id'];
                    $ret['results'][$row['registration_type_id']]['firstname']					= stripslashes($row['firstname']);
                    $ret['results'][$row['registration_type_id']]['lastname']					= stripslashes($row['lastname']);
                    $ret['results'][$row['registration_type_id']]['email']						= stripslashes($row['email']);
                    $ret['results'][$row['registration_type_id']]['reg_email']						= stripslashes($row['reg_email']);
                    $ret['results'][$row['registration_type_id']]['customer_id']						= stripslashes($row['customer_id']);
                    $i++;
                }
                $ret['rows_returned'] = @mysql_num_rows($result);

                $result2 = mysql_query($query2);
                if($result2 && mysql_num_rows($result2)>0) {
                    //$row2 = mysql_fetch_assoc($result2);
                    $ret['found'] = mysql_num_rows($result2);
                }
            }
    ///////////////////////
            */
    $sql3 = $mainSql . " AND " . $oldEmail . " AND " . $termB;
    $qq = "";
    if (strlen($oldEmail) > 0) {
        $qq = "UNION (" . $sql3 . ")";
    }

    $idList = '';
    unset($cIds);
    $mainSql = "$SelectSecondFields
                    $selectFromSqlSecond
                    $mainInitWhere";

    $countSql = "Select count(*) as nr
                    $selectFromSqlSecond
                    $mainInitWhere";
    if ($useEmailUnion === true) {


        $sql1 = $mainSql . $termA;
        $sql2 = $mainSql . $termB;

        $query = "select "
            . "s.id, s.container_id, s.customer_id, s.country, s.zip, s.state, s.city, 
                        s.address2, s.address, s.phone, s.lastname, s.firstname,
                        s.activation_code_id, s.code, s.reg_email, s.email, s.registration_type_id,
                        s.path, s.group_name "
            . "from ((" . $sql1 . ") UNION (" . $sql2 . ") " . $qq . " ) s " . $sqlOrder3 . $limit_sql;

    } elseif ($useRestUnion === true) {
        $sql1 = $mainSql . $termA;

        $query = "select "
            . "s.id, s.container_id, s.customer_id, s.country, s.zip, s.state, s.city, 
                        s.address2, s.address, s.phone, s.lastname, s.firstname,
                        s.activation_code_id, s.code, s.reg_email, s.email, s.registration_type_id,
                        s.path, s.group_name "
            . "from ((" . $sql1 . ") " . $qq . " ) s " . $sqlOrder3 . $limit_sql;
    } elseif ($joinCondition) {
        $query = $mainSql . $termA . $limit_sql;
        $query2 = $mainSql . $termA;


    } else {

        $query = $mainSql . ' AND ' . $startWhere . $cond_sql_second . $endWhere . $oldSql . $customerSql . $endAll . $sqlOrder2 . $limit_sql;
        $query2 = $mainSql . ' AND ' . $startWhere . $cond_sql_second . $endWhere . $oldSql . $customerSql . $endAll . $sqlOrder2;

    }
    //      $query2 = $countSql . ' AND '. $startWhere .$cond_sql_second.$endWhere.$oldSql.$customerSql.$endAll.$sqlOrder2;

    $result = mysql_query($query);
    if ($result && mysql_num_rows($result) > 0) {
        //$ret['rows_returned'] += @mysql_num_rows($result);
        $result2 = mysql_query($query2);
//            if($result2 && mysql_num_rows($result2)>0) {
//                //$row2 = mysql_fetch_assoc($result2);
        $ret['found'] += mysql_num_rows($result2);
//            }
        while ($row = mysql_fetch_assoc($result)) {

            if (isset($row['registration_type_id']) && $row['registration_type_id'] > 0) {
                $arrayKey = $row['reg_email'];
            } else {
                $arrayKey = $row['email'];
            }
            $emailPresent = array_search($arrayKey, $ret['old_customers']['emails']);
            $customerPresent = false;
            if (is_array($ret['old_customers']['customer'])) {
                $customerPresent = array_search($row['customer_id'], $ret['old_customers']['customer']);
            }

            if ($customerPresent) {
                //
                // get old custoemr data
                //
                $oldData = $this->getOldCustomerByCustomerId($customerPresent);

                if (sizeof($oldData) > 0) {
                    $row['firstname'] = $oldData['firstname'];
                    $row['lastname'] = $oldData['lastname'];
                    $row['email'] = $oldData['email'];
                    $row['customer_id'] = $oldData['customer_id'];
                    if (strlen(trim($arrayKey)) < 1) {
                        $arrayKey = $oldData['email'];
                    }
                }
            } elseif ($emailPresent) {
                //
                // get old custoemr data
                //
                $oldData = $this->getOldCustomerByCustomerId($emailPresent);
                if (sizeof($oldData) > 0) {
                    $row['firstname'] = $oldData['firstname'];
                    $row['lastname'] = $oldData['lastname'];
                    $row['city'] = $oldData['city'];
                    $row['state'] = $oldData['state'];
                    $row['email'] = $oldData['email'];
                    $row['customer_id'] = $oldData['customer_id'];
                    if (strlen(trim($arrayKey)) < 1) {
                        $arrayKey = $oldData['email'];
                    }
                }
            } else {
                $oldData = $this->getOldCustomerByCustomerId($row['customer_id']);

                if (sizeof($oldData) > 0) {
                    $row['firstname'] = $oldData['firstname'];
                    $row['lastname'] = $oldData['lastname'];
                    $row['city'] = $oldData['city'];
                    $row['state'] = $oldData['state'];
                    $row['email'] = $oldData['email'];
                    $row['customer_id'] = $oldData['customer_id'];
                    if (strlen(trim($arrayKey)) < 1) {
                        $arrayKey = $oldData['customer_id'];
                    }
                }
            }

            if (strlen(trim($arrayKey)) < 1) {
                // $ret['rows_returned']--;
                // $ret['found']--;
            } else {
                if (trim($ret['results'][$arrayKey]['reg_email']) == '') {
                    $ret['results'][$arrayKey]['reg_email'] = stripslashes($row['reg_email']);
                }
                if (trim($ret['results'][$arrayKey]['registration_type_id']) == '') {
                    $ret['results'][$arrayKey]['registration_type_id'] = $row['registration_type_id'];
                }
                if (trim($ret['results'][$arrayKey]['group_name']) == '') {
                    $ret['results'][$arrayKey]['group_name'] = $row['group_name'];
                }

                if ($emailPresent || $customerPresent) {
                    $ret['results'][$arrayKey]['firstname'] = stripslashes($row['firstname']);
                    $ret['results'][$arrayKey]['lastname'] = stripslashes($row['lastname']);
                    $ret['results'][$arrayKey]['email'] = stripslashes($arrayKey);
                    $ret['results'][$arrayKey]['customer_id'] = stripslashes($row['customer_id']);
                    $ret['results'][$arrayKey]['state'] = stripslashes($row['state']);
                    $ret['results'][$arrayKey]['city'] = stripslashes($row['city']);
                } else {
                    if (trim($ret['results'][$arrayKey]['firstname']) == '') {
                        $ret['results'][$arrayKey]['firstname'] = stripslashes($row['firstname']);
                    }
                    if (trim($ret['results'][$arrayKey]['lastname']) == '') {
                        $ret['results'][$arrayKey]['lastname'] = stripslashes($row['lastname']);
                    }
                    if (trim($ret['results'][$arrayKey]['email']) == '') {
                        $ret['results'][$arrayKey]['email'] = stripslashes($arrayKey);
                    }
                    if (trim($ret['results'][$arrayKey]['customer_id']) == '') {
                        $ret['results'][$arrayKey]['customer_id'] = stripslashes($row['customer_id']);
                    }
                    if (trim($ret['results'][$arrayKey]['state']) == '') {
                        $ret['results'][$arrayKey]['state'] = stripslashes($row['state']);
                    }
                    if (trim($ret['results'][$arrayKey]['city']) == '') {
                        $ret['results'][$arrayKey]['city'] = stripslashes($row['city']);
                    }
                }
            }
        }
        mysql_free_result($result);
        //mysql_free_result($result2);
    }


    $GLOBALS['pagination'] = $this->GetPages($ret['found']);
    //var_dump($GLOBALS['pagination']);die;

    return $ret;
}