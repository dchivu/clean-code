<?php /** @noinspection SqlResolve */


function getConvertProductEntitlements($prodId, $customer_id, $email = '', $isBundle = false)
{
    $products = array();

    $q = "SELECT pp.product_id, pp.quantity, prod.product_name, prod.licenses_nr,
            sg.database, s.software_version, sf.android, sf.ios,
            IF(prod.licenses_nr>0,pp.quantity * prod.licenses_nr,'-1') as entitlements,
            IF(sf.android + sf.ios > 0, 1, 0) as is_mobile, s.gateway_code, sg.id as group_id
            FROM suid.products_to_products pp
            JOIN suid.products prod on pp.product_id=prod.id
            JOIN suid.families_to_products ftp ON ftp.product_id=prod.id
            JOIN suid.software_families sf ON sf.id = ftp.software_family_id
            JOIN suid.software_to_software_families stsf ON stsf.software_family_id = sf.id
            JOIN suid.software s ON stsf.software_id=s.id
            JOIN suid.software_groups sg on s.software_group_id=sg.id
            WHERE pp.parent_id = '" . $prodId . "'";

    //$fhQuota = array();
    $result = mysql_query($q);
    if ($result && mysql_num_rows($result) > 0) {
        //
        // return products array
        //
        while ($row = mysql_fetch_assoc($result)) {

            if ($row['database'] == 'sconlinebackup') {
                $quota = $this->getQuota($row['gateway_code']);
                if (isset($this->fhQuota['upgrade_id'])) {
                    if ($this->fhQuota['upgrade_id'] < $quota['upgrade_id']) {
                        $this->fhQuota = array();
                        $this->fhQuota = $quota;

                    }
                } else {
                    $this->setFhQuota($quota);
                }

                $fhKeyId = $this->getFhKeyId($email);
                $presentQuota = $this->getPresentQuota($fhKeyId);

                if (count($presentQuota) > 0) {
                    $products[$row['database']]['presentQuota'] = $presentQuota;
                }
            }


            if ($row['is_mobile'] == 1) {

                $allInstalls = $this->getInstallsByProduct($row['database'], $row['software_version'], $email, '', $isBundle);
                $products[$row['database']]['mobile'][$row['product_id']] = $row;
                $nonistalledProducts = $this->returnProductsToDelete($customer_id, $email, $row['group_id'], $row['software_version'], $row['database'], $isBundle);

                if (sizeof($allInstalls) > 0) {
                    $products[$row['database']]['mobile'][$row['product_id']]['installations'] = $allInstalls;
                }
                if (sizeof($nonistalledProducts) > 0) {
                    $products[$row['database']]['mobile'][$row['product_id']]['unregistered'] = $nonistalledProducts;
                }
            } else {
                $products[$row['database']]['desktop'][$row['product_id']] = $row;
                if ($row['database'] == 'scpg') {
                    $products[$row['database']]['desktop'][$row['product_id']]['entitlements'] = $row['quantity'];
                }
                $allInstalls = $this->getInstallsByProduct($row['database'], $row['software_version'], $email, '', $isBundle);
                $nonistalledProducts = $this->returnProductsToDelete($customer_id, $email, $row['group_id'], $row['software_version'], $row['database'], $isBundle);

                if (sizeof($allInstalls) > 0) {
                    if ($row['database'] == 'sconlinebackup') {
                        $products[$row['database']]['desktop'][$row['product_id']]['quota'] = $this->getFhQuota();
                        $products[$row['database']]['desktop'][$row['product_id']]['installations'] = $allInstalls;
                        $products[$row['database']]['desktop'][$row['product_id']]['installations']['quota'] = $this->checkInstalledFhQuota($customer_id, $email);
                    } elseif ($row['database'] == 'scpg') {
                        $products[$row['database']]['desktop'][$row['product_id']]['entitlements'] = $row['quantity'];
                        $installs = array();
                        $idList = array();

                        if (sizeof(isset($allInstalls['pg_desktop']) && $allInstalls['pg_desktop']) > 0) {

                            foreach ($allInstalls['pg_desktop'] as $ID => $pgDetails) {
                                $idList[] = $pgDetails['id'];
                                $allInstallsDev['pg_desktop'][$pgDetails['parent_product_id']]['id'] = $pgDetails['id'];
                                $allInstallsDev['pg_desktop'][$pgDetails['parent_product_id']]['parent_product_id'] = $pgDetails['parent_product_id'];
                                $allInstallsDev['pg_desktop'][$pgDetails['parent_product_id']]['activation_code_id'] = $pgDetails['activation_code_id'];
                                $allInstallsDev['pg_desktop'][$pgDetails['parent_product_id']]['family_code'] = 'pg_desktop';
                                $allInstallsDev['pg_desktop'][$pgDetails['parent_product_id']]['devices'][$pgDetails['id']] = array('pc_description' => $pgDetails['pc_description'], 'activation_code_id' => $pgDetails['activation_code_id']);
                            }
                        }

                        $allInstalIds = implode(',', $idList);
                        $products[$row['database']]['desktop'][$row['product_id']]['pg_installs'] = $allInstalIds;
                        $products[$row['database']]['desktop'][$row['product_id']]['installations'] = $allInstallsDev;
                        $products[$row['database']]['desktop'][$row['product_id']]['entitlements'] = $row['quantity'];
                    } else {
                        $products[$row['database']]['desktop'][$row['product_id']]['installations'] = $allInstalls;
                    }

                }

                if (sizeof($nonistalledProducts) > 0) {
                    //
                    // get uninstalled products
                    //
                    //$allProducts = $this->returnProductsToDelete($customer_id, $email, $row['group_id'], $row['software_version'], $row['database']);
                    $products[$row['database']]['desktop'][$row['product_id']]['unregistered'] = $nonistalledProducts;
                    if ($row['database'] == 'sconlinebackup') {
                        if (sizeof($nonistalledProducts) > 0) {
                            // $products[$row['database']]['desktop'][$row['product_id']]['unregistered'][$row['software_version']]['haveOther'] = "1";
                        }
                        $products[$row['database']]['desktop'][$row['product_id']]['unregistered'][$row['software_version']]['quota'] = $this->checkInstalledFhQuota($customer_id, $email);
                    }
                }
            }

        }
    }
    return $products;
}