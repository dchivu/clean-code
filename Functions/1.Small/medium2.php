<?php
function /*array*/ GetPerriods(/*int*/ $user_id)
{
    //echo("\$user_id = $user_id<br/>");
    $ret = array();
    $reg_date = array();
    $ret['int_val'] = array();
    $billtype = "";

    $reg_date = $this->GetRegistrationDate($user_id);
    //$reg_date = array('a_year' => '2000', 'a_month' => '2', 'a_day' => '25');
    //echo("\$reg_date = ");
    //print_r($reg_date);
    //echo("<br/>");
    $now_year = date("Y");
    $now_month = date("m");
    $now_day = date("d");

    //$min_year  = "2005";
    //$min_month = "5";
    //$min_day   = "1";
    $min_year = "2006";
    $min_month = "1";
    $min_day = "1";

    for (
        $i = $reg_date['a_year'];
    ($i <= $now_year);
        $i++
    ) // Start for 1
    {
        if ($i == $min_year) {
            //
            // The year ($i) is in range because is equal with the minimum year.
            //
            $ret['int_val'][$i] = array();

            if ($reg_date['a_year'] == $now_year) {
                //
                // The registration date is equal with current year
                //
                for (
                    $j = $reg_date['a_month'];
                    ($j <= 12) && ($j <= $now_month);
                    $j++
                ) {
                    if ($j >= $min_month) {
                        //
                        // The month is in range.
                        //
                        array_push($ret['int_val'][$i], $j);
                    } else {
                        //
                        // The month is not in range.
                        // Do nothing.
                        //
                    }
                }
                continue;
            }

            if ($reg_date['a_year'] == $i) {
                //
                // The registration date is equal with $i year
                //
                for ($j = $reg_date['a_month']; $j <= 12; $j++) {
                    if ($j >= $min_month) {
                        //
                        // The month is in range.
                        //
                        array_push($ret['int_val'][$i], $j);
                    } else {
                        //
                        // The month is not in range.
                        //
                    }
                }
                continue;
            }

            if ($now_year == $i) {
                //
                // The year $i is equal with current year.
                //
                if ($reg_date['a_month'] >= 1) {
                    for ($j = 1; $j <= $now_month; $j++) {
                        if ($j >= $min_month) {
                            //
                            // The month is in range.
                            //
                            array_push($ret['int_val'][$i], $j);
                        } else {
                            //
                            // The month is not in range.
                            //
                        }
                    }
                }
            } elseif ($now_year > $i) {
                //
                // The current year is bigger than $i year.
                //
                for ($j = 1; $j <= 12; $j++) {
                    if ($j >= $min_month) {
                        //
                        // The month is in range.
                        //
                        array_push($ret['int_val'][$i], $j);
                    } else {
                        //
                        // The month is not in range.
                        //
                    }
                }
            }
        } else {
            //
            // Here is possible the year to not be in range because
            // is not equal with minimum year, so we will test again.
            //
            if ($i > $min_year) {
                //
                // The year is in range beacuse ia bigger than minimun year.
                //

                $ret['int_val'][$i] = array();

                if ($reg_date['a_year'] == $now_year) {
                    //
                    // The year is in range because is equal with current year.
                    //
                    for (
                        $j = $reg_date['a_month'];
                        ($j <= 12) && ($j <= $now_month);
                        $j++
                    ) {
                        if ($j >= $min_month) {
                            //
                            // Month is in range.
                            //
                            array_push($ret['int_val'][$i], $j);
                        } else {
                            //
                            // Month is not in range.
                            //
                        }
                    }
                    continue;
                }

                if ($reg_date['a_year'] == $i) {
                    //
                    // Registration year is equal than $i year.
                    //
                    for ($j = $reg_date['a_month']; $j <= 12; $j++) {
                        array_push($ret['int_val'][$i], $j);
                    }
                    continue;
                }

                if ($now_year == $i) {
                    //
                    // The $i year is equal with current year.
                    //
                    if ($reg_date['a_month'] >= 1) {
                        for ($j = 1; $j <= $now_month; $j++) {
                            array_push($ret['int_val'][$i], $j);
                        }
                    }
                } elseif ($now_year > $i) {
                    //
                    // The current year is bigger than $i year.
                    //
                    for ($j = 1; $j <= 12; $j++) {
                        array_push($ret['int_val'][$i], $j);
                    }
                }
            } else {
                //
                // The year is not in range.
                //
            }
        }
    }// End for 1

    //
    // The string representation of the year is
    // created in the next loop (for).
    //
    $ret['str_val'] = array();
    foreach ($ret['int_val'] as $key => $val) {
        $ret['str_val'][$key] = array();
        foreach ($val as $key2 => $val2) {
            $ret['str_val'][$key][$val2]['title'] =
                date("F Y", mktime(0, 0, 0, $val2, 1, $key));
            $ret['str_val'][$key][$val2]['xdate'] =
                date("Y-m-d", mktime(0, 0, 0, $val2, 1, $key));
        }
    }
    //echo("int GetPerrionds = ");
    //print_r($ret);
    //echo("<br/>");

    return $ret;
}