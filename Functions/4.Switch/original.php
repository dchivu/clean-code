<?php /** @noinspection ALL */

function calculatePay($employee)
{
    switch ($employee) {
        case 'commissioned':
            return calculateCommissionedPay($employee);
        case 'hourly':
            return calculateHourlyPay($employee);
        case 'salaried':
            return calculateSalariedPay($employee);
        default:
            throw new InvalidEmployeeType();
    }
}

//isPayday
//deliverPay