<?php /** @noinspection ALL */

abstract class Employee
{
    public abstract function calculatePay();
    public abstract function isPayday();
    public abstract function deliverPay();
}

class EmployeeFactory
{

    public function makeEmployee($type, $employeeData)
    {
        switch ($type) {
            case 'commissioned':
                return new CommissionedEmployee($employeeData);
            case 'hourly':
                return new HourlyEmployee($employeeData);
            case 'salaried':
                return new SalariedEmployee($employeeData);
            default:
                throw new InvalidEmployeeType();
        }
    }
}