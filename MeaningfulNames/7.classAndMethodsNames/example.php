<?php

// Class names and members - an object, resource, entity
class AddressParser{}
class Product{}
class User{}

//Class methods - an action
//setters and getters
function getName(){};
function setName(){};
function isActive(){};

function removeCustomer(){}
function changeDate(){}
function bindValue(){}

$obj = new stdClass();

class Product{
    private $name;

    /**
     * @return mixed
     */
    public function name()
    {
        return $this->name;
    }

}
