<?php


class Blog
{
    const STATUS_TAG = 'flag';
    const ACTIVE = 1;
    private $allPosts;

    public function getActivePosts()
    {
        $activePosts = [];
        foreach ($this->allPosts as $post) {
            if ($this->isActive($post)) {
                $activePosts[] = $post;
            }
        }
        return $activePosts;
    }

    private function isActive($post)
    {
        return $post[self::STATUS_TAG] === self::ACTIVE;
    }

}
