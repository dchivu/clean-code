<?php

class Duration{
    private $elapsedTimeInDays;
    private $elapsedTimeInMinutes;

    public function getElapsedTimeInDays()
    {
        return $this->elapsedTimeInDays;
    }

    public function getElapsedTimeInMinutes()
    {
        return $this->elapsedTimeInMinutes;
    }
}

(new Duration())->getElapsedTimeInDays();


