<?php


class Blog{
    private $list;

    public function get(): array
    {
        $newList = [];

        foreach ($this->list as $item){
            if($item['flag'] === 1){
                $newList[] = $item;
            }
        }
        return $newList;
    }
}

$blog = new Blog();
$list = $blog->get();