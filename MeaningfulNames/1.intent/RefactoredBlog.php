<?php


class Blog
{
    const FLAG_NAME = 'flag';
    private $allPosts;

    public function getActivePosts()
    {
        $activePosts = [];

        foreach ($this->allPosts as $post) {
            if ($post[self::FLAG_NAME] === 1) {
                $activePosts[] = $post;
            }
        }
        return $activePosts;
    }
}

$blog = new Blog();
$list = $blog->getActivePosts();